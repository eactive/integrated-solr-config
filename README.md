# Integrated solr config #
The Integrated solr config is used in Solr servers for Integrated applications.

## Documentation ##
* [Integrated for developers website](http://www.integratedfordevelopers.com "Integrated for developers website")

## License ##
This bundle is under the MIT license. See the complete license in the bundle:

    LICENSE

## About ##
This repository is part of the Integrated project. You can read more about this project on the
[Integrated for developers](http://www.integratedfordevelopers.com "Integrated for developers") website.